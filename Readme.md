# Modelling dispersal, survival and trapping in SIT trials

Facundo Muñoz · Alexandre Capel

Inrae. Nantes, august 2024.

[Modelling in Animal Health conference](https://modah2024.workshop.inrae.fr/)


This repository contains the source code for the slides presented at the conference, in two versions:

- [Short HTML version](https://astre.gitlab.cirad.fr/presentations/202408-modah-modelling-sit), as presented at the conference

- [Extended HTML version](https://astre.gitlab.cirad.fr/presentations/202408-modah-modelling-sit/extended.html), with a few additional slides with more details

- [Extended and annotated HTML version](https://astre.gitlab.cirad.fr/presentations/202408-modah-modelling-sit/extended-annotated.html), extended version including speaker notes

In either version, you can access the speaker notes by pressing 's', which reveals explanatory comments associated to each slide.


The results are pre-computed and are not reproducible with the materials in this repository alone